<?php

namespace Terrazza\Component\ReflectionClass\ClassName;
use RuntimeException;

class ReflectionClassClassNameException extends RuntimeException {}