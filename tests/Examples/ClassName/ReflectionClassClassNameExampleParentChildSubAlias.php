<?php
namespace Terrazza\Component\ReflectionClass\Tests\Examples\ClassName;
use Terrazza\Component\ReflectionClass\Tests\Examples\ClassName\Sub as subItem;
class ReflectionClassClassNameExampleParentChildSubAlias {
    public subItem\ReflectionClassClassNameExampleSubItem $simpleItem;
}